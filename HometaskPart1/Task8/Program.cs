﻿// See https://aka.ms/new-console-template for more information
/*
     * Немає перевірки на валідність введених даних.
     * Особливо це стосується коми!!!
     */
Console.WriteLine("Write coordinate of point.");
Console.WriteLine("(Press enter to confirm.)");
Console.WriteLine("(Use , as a separator.)");
Console.Write("X = ");
double x = Convert.ToDouble(Console.ReadLine());
Console.Write("Y = ");
double y = Convert.ToDouble(Console.ReadLine());
int quater = (x > 0) ? 2 : 1;
quater = (y > 0) ? quater + 2 : quater + 0;
Console.WriteLine($"Point coordinate: ({x},{y})");
switch (quater)
{
    case 1:
        Console.WriteLine(" | ");
        Console.WriteLine("---");
        Console.WriteLine("*| ");
        Console.WriteLine("Point in III quater.");
        break;
    case 2:
        Console.WriteLine(" | ");
        Console.WriteLine("---");
        Console.WriteLine(" |*");
        Console.WriteLine("Point in IV quater.");
        break;
    case 3:
        Console.WriteLine("*| ");
        Console.WriteLine("---");
        Console.WriteLine(" | ");
        Console.WriteLine("Point in II quater.");
        break;
    case 4:
        Console.WriteLine(" |*");
        Console.WriteLine("---");
        Console.WriteLine(" | ");
        Console.WriteLine("Point in I quater.");
        break;
}
