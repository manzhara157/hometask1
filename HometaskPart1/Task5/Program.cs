﻿// See https://aka.ms/new-console-template for more information
/*
     * Немає перевірки на валідність введених даних.
     * Особливо це стосується коми!!!
     */
Console.WriteLine("Write 2 number.");
Console.WriteLine("(Press enter to confirm.)");
Console.WriteLine("(Use , as a separator.)");
Console.Write("A = ");
double a = Convert.ToDouble(Console.ReadLine());
Console.Write("B = ");
double b = Convert.ToDouble(Console.ReadLine());
double c = (a < b) ? b : a;
a = (a < b) ? a : b;
b = c;
Console.WriteLine($"A = {a}, B = {b}.");