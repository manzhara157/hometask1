﻿// See https://aka.ms/new-console-template for more information
/* 
     * Тут не передбачена переірка на валідність введених даних.
     * Нуль приймається за від'ємне значення.
     * Реалызація не гнучка, так як заточена під 3 числа.
     * Можна було зробити через цикли та масиви, але для 
     * трьох значень це не доцільно.
     */
int negative = 0, positive = 0;
Console.WriteLine("Write 3 number.");
Console.WriteLine("(Press enter to confirm.)");
int number1 = Convert.ToInt16(Console.ReadLine());
int number2 = Convert.ToInt16(Console.ReadLine());
int number3 = Convert.ToInt16(Console.ReadLine());
positive = (number1 > 0) ? positive + 1 : positive;
positive = (number2 > 0) ? positive + 1 : positive;
positive = (number3 > 0) ? positive + 1 : positive;
negative = 3 - positive;
Console.WriteLine($"Nagative number: {negative}, Positive number: {positive}.");