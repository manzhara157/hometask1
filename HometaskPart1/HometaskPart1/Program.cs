﻿// See https://aka.ms/new-console-template for more information

Console.WriteLine("Choose a task.");
Console.WriteLine("To choose a task, enter number form 3 to 10:");
int task = Convert.ToInt16(Console.ReadLine());

switch (task) {
    case 3:
        Task3();
        break;
    case 4:
        Task4();
        break;
    case 5:
        Task5();
        break;
    case 6:
        Task6();
        break;
    case 7:
        Task7();
        break;
    case 8:
        Task8();
        break;
    case 9:
        Task9(300);
        Task9(1300);
        Task9(1900);
        Task9(1200);
        Task9(2000);
        Task9(2004);
        Task9(2008);
        Task9(2012);
		Console.WriteLine("Write your year.");
	    Console.WriteLine("(Press enter to confirm.)");
		int year = Convert.ToInt32(Console.ReadLine());
		Task9(year);
        break;
    default: Console.WriteLine("Incorrectly entered the task number");
        break;
}

void Task3()
{
    /* 
     * Тут не передбачена переірка на валідність введених даних.
     * Нуль приймається за від'ємне значення.
     * Реалызація не гнучка, так як заточена під 3 числа.
     * Можна було зробити через цикли та масиви, але для 
     * трьох значень це не доцільно.
     */
    int negative = 0, positive = 0;
    Console.WriteLine("Write 3 number.");
    Console.WriteLine("(Press enter to confirm.)");
    int number1 = Convert.ToInt16(Console.ReadLine());
    int number2 = Convert.ToInt16(Console.ReadLine());
    int number3 = Convert.ToInt16(Console.ReadLine());
    positive = (number1 > 0) ? positive + 1 : positive;
    positive = (number2 > 0) ? positive + 1 : positive;
    positive = (number3 > 0) ? positive + 1 : positive;
    negative = 3 - positive;
    Console.WriteLine($"Nagative number: {negative}, Positive number: {positive}.");
}

void Task4()
{
    /*
     * Немає перевірки на валідність введених даних.
     */
    Console.WriteLine("Write 2 number.");
    Console.WriteLine("(Press enter to confirm.)");
    int number1 = Convert.ToInt16(Console.ReadLine());
    int number2 = Convert.ToInt16(Console.ReadLine());
    int smaller = (number1 < number2) ? number1 : number2;
    int biggest = (number1 > number2) ? number1 : number2;
    Console.WriteLine($"Biggest number: {biggest}, smaller number: {smaller}.");
}

void Task5()
{
    /*
     * Немає перевірки на валідність введених даних.
     * Особливо це стосується коми!!!
     */
    Console.WriteLine("Write 2 number.");
    Console.WriteLine("(Press enter to confirm.)");
    Console.WriteLine("(Use , as a separator.)");
    Console.Write("A = ");
    double a = Convert.ToDouble(Console.ReadLine());
    Console.Write("B = ");
    double b = Convert.ToDouble(Console.ReadLine());
    double c = (a < b) ? b : a;
    a = (a < b) ? a : b;
    b = c;
    Console.WriteLine($"A = {a}, B = {b}.");
}

void Task6()
{
    /*
     * Немає перевірки на валідність введених даних.
     * Особливо це стосується коми!!!
     */
    Console.WriteLine("Write 3 number.");
    Console.WriteLine("(Press enter to confirm.)");
    Console.WriteLine("(Use , as a separator.)");
    Console.Write("A = ");
    double a = Convert.ToDouble(Console.ReadLine());
    Console.Write("B = ");
    double b = Convert.ToDouble(Console.ReadLine());
    Console.Write("C = ");
    double c = Convert.ToDouble(Console.ReadLine());
	bool isUp = (a < b) ? (b < c) ? true : false : false;
	bool isDown = (a > b) ? (b > c) ? true : false : false;
	if (isUp || isDown) {
		a = a * 2;
		b = b * 2;
		c = c * 2;
	} else {
		a = a * -1;
		b = b * -1;
		c = c * -1;
	}
    Console.WriteLine($"A = {a}, B = {b}, C = {c}.");
}

void Task7()
{
    /*
     * Немає перевірки на валідність введених даних.
     * Особливо це стосується коми!!!
     */
    Console.WriteLine("Write coordinate of 3 points.");
    Console.WriteLine("(Press enter to confirm.)");
    Console.WriteLine("(Use , as a separator.)");
    Console.Write("A = ");
    double a = Convert.ToDouble(Console.ReadLine());
    Console.Write("B = ");
    double b = Convert.ToDouble(Console.ReadLine());
    Console.Write("C = ");
    double c = Convert.ToDouble(Console.ReadLine());
	double distanceAB = Math.Abs(a - b);
	double distanceAC = Math.Abs(a - c);
	bool isAB = (distanceAB < distanceAC) ? true : false;
	if (isAB) {
		Console.WriteLine($"Point B is nearly to A. Distance = {distanceAB}.");
	} else {
		Console.WriteLine($"Point C is nearly to A. Distance = {distanceAC}.");
	}
}

void Task8()
{
    /*
     * Немає перевірки на валідність введених даних.
     * Особливо це стосується коми!!!
     */
    Console.WriteLine("Write coordinate of point.");
    Console.WriteLine("(Press enter to confirm.)");
    Console.WriteLine("(Use , as a separator.)");
    Console.Write("X = ");
    double x = Convert.ToDouble(Console.ReadLine());
    Console.Write("Y = ");
    double y = Convert.ToDouble(Console.ReadLine());
	int quater = (x > 0) ? 2 : 1;
	quater = (y > 0) ? quater + 2 : quater + 0;
	Console.WriteLine($"Point coordinate: ({x},{y})");
    switch (quater)
    {
        case 1:
            Console.WriteLine(" | ");
            Console.WriteLine("---");
            Console.WriteLine("*| ");
            Console.WriteLine("Point in III quater.");
            break;
        case 2:
            Console.WriteLine(" | ");
            Console.WriteLine("---");
            Console.WriteLine(" |*");
            Console.WriteLine("Point in IV quater.");
            break;
        case 3:
            Console.WriteLine("*| ");
            Console.WriteLine("---");
            Console.WriteLine(" | ");
            Console.WriteLine("Point in II quater.");
            break;
        case 4:
            Console.WriteLine(" |*");
            Console.WriteLine("---");
            Console.WriteLine(" | ");
            Console.WriteLine("Point in I quater.");
            break;
    }
}

void Task9(int year)
{
    /*
     * Немає перевірки на валідність введених даних.
     */
    bool isLeap = (year%4 == 0) ? true : false;
	if (isLeap) {
		if ((year%100 != 0) || (year%400 == 0)) {
			Console.WriteLine($"Year {year} has 366 days.");
		} else {
			Console.WriteLine($"Year {year} has 365 days.");
		}
	} else {
		Console.WriteLine($"Year {year} has 365 days.");
	}

}
