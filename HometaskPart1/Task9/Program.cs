﻿// See https://aka.ms/new-console-template for more information
/*
     * Немає перевірки на валідність введених даних.
     */
Console.WriteLine("Write your year.");
Console.WriteLine("(Press enter to confirm.)");
int year = Convert.ToInt32(Console.ReadLine());
bool isLeap = (year % 4 == 0) ? true : false;
if (isLeap)
{
	if ((year % 100 != 0) || (year % 400 == 0))
	{
		Console.WriteLine($"Year {year} has 366 days.");
	}
	else
	{
		Console.WriteLine($"Year {year} has 365 days.");
	}
}
else
{
	Console.WriteLine($"Year {year} has 365 days.");
}
