﻿// See https://aka.ms/new-console-template for more information
/*
     * Немає перевірки на валідність введених даних.
     */
Console.WriteLine("Write 2 number.");
Console.WriteLine("(Press enter to confirm.)");
int number1 = Convert.ToInt16(Console.ReadLine());
int number2 = Convert.ToInt16(Console.ReadLine());
int smaller = (number1 < number2) ? number1 : number2;
int biggest = (number1 > number2) ? number1 : number2;
Console.WriteLine($"Biggest number: {biggest}, smaller number: {smaller}.");
