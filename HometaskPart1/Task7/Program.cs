﻿// See https://aka.ms/new-console-template for more information
/*
     * Немає перевірки на валідність введених даних.
     * Особливо це стосується коми!!!
     */
Console.WriteLine("Write coordinate of 3 points.");
Console.WriteLine("(Press enter to confirm.)");
Console.WriteLine("(Use , as a separator.)");
Console.Write("A = ");
double a = Convert.ToDouble(Console.ReadLine());
Console.Write("B = ");
double b = Convert.ToDouble(Console.ReadLine());
Console.Write("C = ");
double c = Convert.ToDouble(Console.ReadLine());
double distanceAB = Math.Abs(a - b);
double distanceAC = Math.Abs(a - c);
bool isAB = (distanceAB < distanceAC) ? true : false;
if (isAB)
{
    Console.WriteLine($"Point B is nearly to A. Distance = {distanceAB}.");
}
else
{
    Console.WriteLine($"Point C is nearly to A. Distance = {distanceAC}.");
}
