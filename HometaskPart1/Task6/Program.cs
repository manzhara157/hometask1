﻿// See https://aka.ms/new-console-template for more information
/*
     * Немає перевірки на валідність введених даних.
     * Особливо це стосується коми!!!
     */
Console.WriteLine("Write 3 number.");
Console.WriteLine("(Press enter to confirm.)");
Console.WriteLine("(Use , as a separator.)");
Console.Write("A = ");
double a = Convert.ToDouble(Console.ReadLine());
Console.Write("B = ");
double b = Convert.ToDouble(Console.ReadLine());
Console.Write("C = ");
double c = Convert.ToDouble(Console.ReadLine());
bool isUp = (a < b) ? (b < c) ? true : false : false;
bool isDown = (a > b) ? (b > c) ? true : false : false;
if (isUp || isDown)
{
    a = a * 2;
    b = b * 2;
    c = c * 2;
}
else
{
    a = a * -1;
    b = b * -1;
    c = c * -1;
}
Console.WriteLine($"A = {a}, B = {b}, C = {c}.");
